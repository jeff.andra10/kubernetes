## Kubernetes

<p><a href="http://kubernetes.io/">Kubernetes</a> é uma ferramenta OpenSource que faz a gestão de aplicações em containers através de recursos como deployments, updates, scaling e lifecycles, é mantido pela <a href="https://www.cncf.io/">Cloud Native Computing Foundation</a></p>

<h2><a class="anchor" aria-hidden="true" id="kubernetes-ou-solucão-de-cloud-provider"></a><a href="#kubernetes-ou-solucão-de-cloud-provider" aria-hidden="true" class="hash-link"><svg class="hash-link-icon" aria-hidden="true" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Kubernetes ou Solução de Cloud Provider</h2>
<p>Vamos bater um papo agora sobre as motivações em orientar nossa plataforma ao Kubernetes e não a um Cloud Provider em específico:</p>
<ul>
<li>Comunidade OpenSource e CNCF;</li>
<li>Não temos lock-in no provider;</li>
<li><a href="https://aws.amazon.com/pt/eks/">AWS EKS</a>, <a href="https://azure.microsoft.com/pt-br/services/kubernetes-service/">Azure AKS</a> , <a href="https://www.digitalocean.com/products/kubernetes/">Digital Ocean Kubernetes</a>, <a href="https://www.ibm.com/cloud/container-service">IBM Cloud Kubernetes Service</a>;</li>
</ul>
<h2><a class="anchor" aria-hidden="true" id="instalacão"></a><a href="#instalacão" aria-hidden="true" class="hash-link"><svg class="hash-link-icon" aria-hidden="true" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Instalação</h2>
<p>Para instalarmos e iniciarmos nosso cluster, iremos utilizar um toolbox chamado <strong>kubeadm</strong> que irá nos ajudar a configurar nosso cluster atendendo os <a href="https://kubernetes.io/blog/2017/10/software-conformance-certification">requisitos de compliance</a> da ferramenta.<br>
Vocês verão que o processo é simples, e está descrito de maneira bem detalhada na <a href="https://kubernetes.io/docs/setup/independent/install-kubeadm/">documentação oficial</a></p>
<ol>
<li>Checar os <a href="https://kubernetes.io/docs/setup/independent/install-kubeadm/#before-you-begin">Pré Requisitos</a> (e não esquecer de desativar a SWAP)</li>
<li>Configuração do <a href="https://kubernetes.io/docs/setup/independent/install-kubeadm/#installing-runtime">Runtime</a> em nosso caso, o <a href="https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository">Docker</a></li>
</ol>
<pre><code class="hljs"># Conforme Executado <span class="hljs-keyword">no</span> Bootcamp
# Missao <span class="hljs-number">05</span> &gt; Orquestracao <span class="hljs-keyword">e</span> Kubernetes &gt; Instalacao

sudo apt-<span class="hljs-built_in">get</span> <span class="hljs-keyword">update</span>
sudo apt-<span class="hljs-built_in">get</span> install -<span class="hljs-keyword">y</span> apt-transport-https <span class="hljs-keyword">ca</span>-certificates curl software-properties-common
sudo curl -fsSL http<span class="hljs-variable">s:</span>//download.docker.<span class="hljs-keyword">com</span>/linux/ubuntu/gpg | sudo apt-key <span class="hljs-built_in">add</span> -
sudo <span class="hljs-built_in">add</span>-apt-repository <span class="hljs-string">"deb [arch=amd64] https://download.docker.com/linux/ubuntu \$(lsb_release -cs) stable"</span>
sudo apt-<span class="hljs-built_in">get</span> <span class="hljs-keyword">update</span>
sudo apt-<span class="hljs-built_in">get</span> install -<span class="hljs-keyword">y</span> docker-<span class="hljs-keyword">ce</span>=<span class="hljs-number">18.06</span>.<span class="hljs-number">1</span>~<span class="hljs-keyword">ce</span>~<span class="hljs-number">3</span>-<span class="hljs-number">0</span>~ubuntu

sudo usermod -aG docker \$USER
</code></pre>

<ol start="3">
<li>Instalações: <a href="https://kubernetes.io/docs/setup/independent/install-kubeadm/#installing-kubeadm-kubelet-and-kubectl">Kubelet, kubeadm e kubectl</a></li>
</ol>
<pre><code class="hljs"># Conforme Executado <span class="hljs-keyword">no</span> Bootcamp
# Missao <span class="hljs-number">05</span> &gt; Orquestracao <span class="hljs-keyword">e</span> Kubernetes &gt; Instalacao

sudo su
curl -s http<span class="hljs-variable">s:</span>//packages.cloud.google.<span class="hljs-keyword">com</span>/apt/doc/apt-key.gpg | apt-key <span class="hljs-built_in">add</span> -
<span class="hljs-keyword">cat</span> &lt;&lt;EOF &gt;/etc/apt/sources.<span class="hljs-keyword">list</span>.d/kubernetes.<span class="hljs-keyword">list</span>
<span class="hljs-keyword">deb</span> http<span class="hljs-variable">s:</span>//apt.kubernetes.io/ kubernetes-xenial main
EOF
apt-<span class="hljs-built_in">get</span> <span class="hljs-keyword">update</span>
apt-<span class="hljs-built_in">get</span> install -<span class="hljs-keyword">y</span> kubelet=<span class="hljs-number">1.11</span>.<span class="hljs-number">3</span>-<span class="hljs-number">00</span> kubeadm=<span class="hljs-number">1.11</span>.<span class="hljs-number">3</span>-<span class="hljs-number">00</span> kubectl=<span class="hljs-number">1.11</span>.<span class="hljs-number">3</span>-<span class="hljs-number">00</span>
apt-<span class="hljs-keyword">mark</span> hold kubelet kubeadm kubectl
<span class="hljs-keyword">exit</span>
</code></pre>

<h2><a class="anchor" aria-hidden="true" id="iniciando-cluster"></a><a href="#iniciando-cluster" aria-hidden="true" class="hash-link"><svg class="hash-link-icon" aria-hidden="true" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Iniciando Cluster</h2>
<p>Com tudo pronto, hora de ligar os motores e <a href="https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/">iniciar nosso cluster</a>.</p>
<ol>
<li><a href="https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#initializing-your-master">Iniciando o node master</a></li>
</ol>
<pre><code class="hljs"># Conforme Executado no Bootcamp
# Missao <span class="hljs-number">05</span> &gt; Orquestracao e Kubernetes &gt; Iniciando Cluster

sudo kubeadm init --pod-network-cidr=<span class="hljs-number">10.244</span><span class="hljs-number">.0</span><span class="hljs-number">.0</span>/<span class="hljs-number">16</span>
</code></pre>

<h2><a class="anchor" aria-hidden="true" id="configurando-client-kubectl"></a><a href="#configurando-client-kubectl" aria-hidden="true" class="hash-link"><svg class="hash-link-icon" aria-hidden="true" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Configurando Client (kubectl)</h2>
<ol>
<li><a href="https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#more-information">Configurando kubectl</a></li>
</ol>
<pre><code class="hljs"><span class="hljs-comment"># Conforme Executado no Bootcamp</span>
<span class="hljs-comment"># Missao 05 &gt; Orquestracao e Kubernetes &gt; Configurando Client (kubectl)</span>

mkdir -p <span class="hljs-variable">$HOME</span>/.kube
sudo cp -i /etc/kubernetes/admin.conf <span class="hljs-variable">$HOME</span>/.kube<span class="hljs-built_in">/config
</span>sudo chown $(id -u):$(id -g) <span class="hljs-variable">\$HOME</span>/.kube<span class="hljs-built_in">/config
</span></code></pre>

<h2><a class="anchor" aria-hidden="true" id="configuracões-adicionais"></a><a href="#configuracões-adicionais" aria-hidden="true" class="hash-link"><svg class="hash-link-icon" aria-hidden="true" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Configurações Adicionais</h2>
<ol>
<li>Instalando <a href="https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#pod-network">Pod Network Addon</a></li>
<li><a href="https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/#master-isolation">Master Isolation</a></li>
</ol>
<pre><code class="hljs"><span class="hljs-comment"># Conforme Executado no Bootcamp</span>
<span class="hljs-comment"># Missao 05 &gt; Orquestracao e Kubernetes &gt; Configuracoes Adicionais</span>
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/v0.<span class="hljs-number">10.0</span>/Documentation/kube-flannel.yml
kubectl taint nodes --all <span class="hljs-keyword">node</span><span class="hljs-title">-role</span>.kubernetes.io/<span class="hljs-literal">master</span>-
</code></pre>
<h2><a class="anchor" aria-hidden="true" id="executando-nosso-primeiro-servico"></a><a href="#executando-nosso-primeiro-servico" aria-hidden="true" class="hash-link"><svg class="hash-link-icon" aria-hidden="true" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Executando Nosso Primeiro Serviço</h2>
<p><img src="https://d33wubrfki0l68.cloudfront.net/152c845f25df8e69dd24dd7b0836a289747e258a/4a1d2/docs/tutorials/kubernetes-basics/public/images/module_02_first_app.svg" alt="Deployment"></p>
<p><code>kubectl run kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1 --port=8080</code>  <br>
Por padrão todos nossos deployments são acessados apenas da rede privada, interna ao nosso cluster.<br>
Através do kubectl + API conseguimos informações sobre o ambiente e seus recursos, mas não ao serviço propriamente dito.<br>
<code>kubectl get deployment</code></p>
<p>Mas, como acessar esse serviço? O que ele retorna?
Podemos fazer isso via Proxy: (Veremos outras maneiras em breve)<br>
Execute <code>kubectl proxy</code></p>
<p>E faça uma requisição:<br>
<code>curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/</code></p>
<p>Com isso já entendemos o funcionamento básico de um deployment, vamos olhá-lo sob uma nova ótica!</p>
<h3><a class="anchor" aria-hidden="true" id="deploy-via-arquivo-estruturado-yaml"></a><a href="#deploy-via-arquivo-estruturado-yaml" aria-hidden="true" class="hash-link"><svg class="hash-link-icon" aria-hidden="true" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Deploy via Arquivo Estruturado (YAML)</h3>
<p>Essa nossa execução, gerou em nosso cluster um recurso estruturado, podemos acessá-lo através de:<br>
<code>kubectl get deploy kubernetes-bootcamp -o yaml</code>  <br>
Ou até mesmo editá-lo em tempo real:<br>
<code>kubectl edit deploy kubernetes-bootcamp</code></p>
<h2><a class="anchor" aria-hidden="true" id="kubernetes-overview"></a><a href="#kubernetes-overview" aria-hidden="true" class="hash-link"><svg class="hash-link-icon" aria-hidden="true" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Kubernetes Overview</h2>
<p>Agora que já tivemos um pequeno-breve contato com o kubernetes, as coisas vão começar a ficar bem interessantes, mas, precisamos de bagagem, vamos entender e ter claro como o Kubernetes trabalha!</p>

<p><strong>Namespaces</strong>  <br>
É a segregação lógica de nosso cluster, permitem criar divisões de 'ambientes' dentro do nosso cluster.<br>
O que nos possibilita, por exemplo, ter N ambientes lógicos como Desenvolvimento, Homologação e Produção, ou seja, são fatias de nosso cluster físico.<br>
Tudo vive dentro de um namespace, os recursos do kubernetes nascem no namespace <code>kube-system</code>, e para nós, (caso nenhum NS seja definido) tudo é enviado ao NS <code>default</code>.</p>
<p><strong>Deployment (Deploy)</strong>  <br>
Afinal, criamos um deploy via linha de comando e também via arquivo YAML, vimos também que é possível editar esse recurso em tempo real.
Mas, o que aconteceu quando criamos nosso deploy? Como realmente funciona o Kubernetes por trás dos panos?</p>
<p><strong>ReplicaSet</strong>  <br>
Solicitado o Deploy, o Kubernetes imediatamente cria um <strong>Replica Set</strong>, que é o recurso responsável por monitorar e manter a quantidade de execuções(pod) por nós informadas.<br>
<a href="https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/#when-to-use-a-replicaset">Não se preocupem em manter ou alterar um ReplicaSet</a> , depois elas são controladas pelo próprio deployment, considere então o ReplicaSet um recurso operacional do nosso Deployment.<br>
Ou seja, podemos excluir um pod a qualquer momento, o ReplicaSet estará de olho!</p>
<p><strong>POD</strong>  <br>
E falando nele, aí está o POD, rapaz esse que não costuma andar sozinho, está sempre companhado com seus 'N irmãos gêmeos!<br>
POD é onde estão executando nossos containers (sim, um POD, pode ter mais de um container). Por 'viverem' juntos, é garantido que esses containers compartilhem os mesmos recursos.</p>
<p><strong>Services</strong></p>
<p>Vamos utilizar a própria <a href="https://kubernetes.io/docs/concepts/services-networking/service/">documentação</a> para explorar esse recurso!</p>

<h2><a class="anchor" aria-hidden="true" id="lembretes-importantes"></a><a href="#lembretes-importantes" aria-hidden="true" class="hash-link"><svg class="hash-link-icon" aria-hidden="true" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Lembretes Importantes</h2>
<p><code>kubectl apply -f &lt;filename&gt;</code> : Aplica um arquivo de configuração ao server.<br>
<code>kubectl get all --all-namespaces</code> : Lista todos recursos em todos namespaces.</p>
<hr>
<h2><a class="anchor" aria-hidden="true" id="dashboard"></a><a href="#dashboard" aria-hidden="true" class="hash-link"><svg class="hash-link-icon" aria-hidden="true" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>Dashboard</h2>
<p>Vamos começar com o deploy de um dashboard, para quem prefere uma UI com mais recursos que o terminal! :)<br>
<code>Atenção! Atualizado em Jan/2019</code></p>
<pre><code class="hljs css language-shell">kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
</code></pre>
<p>Vamos agora <strong>expor nosso Dashboard</strong>:<br>
<code>kubectl expose deployment kubernetes-dashboard --name=kubernetes-dashboard-nodeport --port=443 --target-port=8443 --type=NodePort -n kube-system</code></p>
<p>Pronto! <strong>Hora de acessar</strong>!</p>
<pre><code class="hljs css language-shell"><span class="hljs-meta">#</span><span class="bash"> Criando Service Account e associando permissao <span class="hljs-string">'cluster-admin'</span></span>
kubectl create serviceaccount kubeadmin -n kube-system 
kubectl create clusterrolebinding kubeadmin-binding --clusterrole=cluster-admin --serviceaccount=kube-system:kubeadmin

kubectl describe sa kubeadmin -n kube-system
kubectl get secret &lt;TOKEN-ID&gt; -n kube-system -o yaml
echo `echo &lt;TOKEN&gt; | base64 --decode`
</code></pre>

<p><code>#showMeTheCode</code>  <br>
️<br>
A mesma coisa, via <strong>arquivo yaml</strong></p>
